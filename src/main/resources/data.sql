--Compañia
INSERT INTO COMPANY (adress, city , country , departament, doc_number , full_name, phone, type_document,
doc_number_represent,type_document_represent,name_represent,adress_represent,city_represent,departament_represent,country_represent,phone_represent)
Values ( 'Calle 123', 'Bogota' , 'Bogota' , 'Bogota D.C', 101222525 , 'Ferrari', '346458', 1 ,
1013618848, 2,'nombre Representante 1', 'Calle falsa 123','soacha','Cundinamarca','Colombia','3115246242');
INSERT INTO COMPANY (adress, city , country , departament, doc_number , full_name, phone, type_document ,
doc_number_represent,type_document_represent,name_represent,adress_represent,city_represent,departament_represent,country_represent,phone_represent)
Values ( 'Calle 56 diagonal 52', 'Bogota' , 'Bogota' , 'Bogota D.C', 2425242525 , 'Mazda S.A', '3698547', 1,
1013612332, 2,'nombre Representante 1', 'Calle falsa 123','soacha','Cundinamarca','Colombia','3115246242');

--vehiculos
INSERT into VEHICLE(CAR_PLATE,BRAND,CHASSIS,ENROLLMENT_DATE,DRY_WEIGHT,GROSS_WEIGHT,LINE,MODEL,MOTOR,NUMBER_OF_DOORS,SEATED_PASSENGERS,STANDING_PASSENGERS,COMPANY_DOC_NUMBER ) 
Values ('thy-768','brand1','chasis1','19910101','56kg','65kg','linea1','motorkasjsa',56,565,25,25,101222525);

INSERT INTO DRIVER (adress, city , country , departament, doc_number , full_name, phone, type_document,vehicle_car_plate )
Values ( 'Calle 123', 'Bogota' , 'Bogota' , 'Bogota D.C', 101361238847 , 'Yhon Alex benavides sanabria', '3678470', 1,'thy-768' );
INSERT INTO DRIVER (adress, city , country , departament, doc_number , full_name, phone, type_document,vehicle_car_plate )
Values ( 'Calle 56 diagonal 52', 'Bogota' , 'Bogota' , 'Bogota D.C', 101361232342 , 'ricardo quevedo reyes', '3698547', 1,'thy-768' );
