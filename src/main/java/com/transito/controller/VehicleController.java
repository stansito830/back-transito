package com.transito.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.transito.dao.VehicleRepository;
import com.transito.entity.Driver;
import com.transito.entity.Vehicle;
@RestController
public class VehicleController {
	@Autowired
	private VehicleRepository repository;
	@PostMapping("/saveVehicle")
	public String saveVehicle(@RequestBody Vehicle vehicle) {
		repository.save(vehicle);
		return "Conductor guardado";	
	}
	@GetMapping("/getVehicle")
	public List<Vehicle> getAll(){
		return repository.findAll();
		
	}
	@GetMapping("/getVehicle/{id}")
	public Optional<Vehicle> getVehicleById(@PathVariable String id) {
		return repository.findById(id);
	}
}
