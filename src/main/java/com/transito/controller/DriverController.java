package com.transito.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.transito.dao.DriverRepository;
import com.transito.entity.Driver;


@RestController
@CrossOrigin

public class DriverController {
@Autowired
private DriverRepository repository;
	
	@PostMapping("/saveDriver")
	public String saveDriver(@RequestBody Driver driver) {
		repository.save(driver);
		return "Conductor guardado";	
	}
	
	@GetMapping("/getDriver")
	public List<Driver> getAll(){
		return repository.findAll();
		
	}

	@GetMapping("/getDriver/{id}")
	public List<Driver> getEmployeesByDept(@PathVariable Long id) {
		return repository.findById(id);
	}
}
