package com.transito.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.transito.dao.CompanyRepository;
import com.transito.entity.Company;

@RestController
public class CompanyController {
@Autowired
private CompanyRepository repository;
	
	@PostMapping("/saveCompany")
	public String saveCompany(@RequestBody Company company) {
		repository.save(company);
		return "Conductor guardado";	
	}
	@GetMapping("/getCompany")
	public List<Company> getAll(){
		return repository.findAll();
		
	}

	@GetMapping("/getCompany/{id}")
	public List<Company> getEmployeesByDept(@PathVariable Long id) {
		return repository.findById(id);
	}
}
