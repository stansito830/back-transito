package com.transito.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(name = "DRIVER")
public class Driver {
	 	@Id
		@Column(name = "DocNumber", length = 150, nullable = false)
	    private Long id;       
	 
	    @Column(name = "TypeDocument", nullable = false)
	    private Long typeDocument;
	 
	    @Column(name = "FullName", length = 64, nullable = false)
	    private String fullName;
	    
	    @Column(name = "Adress", length = 64, nullable = false)
	    private String adress;
	      
	    @Column(name = "City", length = 64, nullable = false)
	    private String city;
	    
	    @Column(name = "Departament", length = 64, nullable = false)
	    private String departament;
	    
	    @Column(name = "Country", length = 64, nullable = false)
	    private String country;
	    
	    @Column(name = "Phone", length = 64, nullable = false)
	    private String phone;
	
		 @ManyToOne()
		 @NotNull
		 public Vehicle vehicle;
	    
		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public Long getTypeDocument() {
			return typeDocument;
		}

		public void setTypeDocument(Long typeDocument) {
			this.typeDocument = typeDocument;
		}

		public String getFullName() {
			return fullName;
		}

		public void setFullName(String fullName) {
			this.fullName = fullName;
		}

		public String getAdress() {
			return adress;
		}

		public void setAdress(String adress) {
			this.adress = adress;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getDepartament() {
			return departament;
		}

		public void setDepartament(String departament) {
			this.departament = departament;
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		public Vehicle getVehicle() {
			return vehicle;
		}

		public void setVehicle(Vehicle vehicle) {
			this.vehicle = vehicle;
		}
	    
	    

}
