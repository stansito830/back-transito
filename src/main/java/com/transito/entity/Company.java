package com.transito.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
 
@Entity
@Table(name = "COMPANY")
public class Company {
    @Id
	@Column(name = "DocNumber", length = 150, nullable = false)
    private Long id;       
 
    @Column(name = "TypeDocument", nullable = false)
    private Long typeDocument;
 
    @Column(name = "FullName", length = 64, nullable = false)
    private String fullName;
    
    @Column(name = "Adress", length = 64, nullable = false)
    private String adress;
      
    @Column(name = "City", length = 64, nullable = false)
    private String city;
    
    @Column(name = "Departament", length = 64, nullable = false)
    private String departament;
    
    @Column(name = "Country", length = 64, nullable = false)
    private String country;
    
    @Column(name = "Phone", length = 64, nullable = false)
    private String phone;

    @Column(name = "DocNumberRepresent", length = 150, nullable = false)
    private Long docNumberRepresent;       
 
    @Column(name = "TypeDocumentRepresent", nullable = false)
    private Long typeDocumentRepresent;
    
    @Column(name = "NameRepresent", length = 64, nullable = false)
    private String nameRepresent;
    
    @Column(name = "AdressRepresent", length = 64, nullable = false)
    private String adressRepresent;
    
    @Column(name = "CityRepresent", length = 64, nullable = false)
    private String cityRepresent;
    
    @Column(name = "DepartamentRepresent", length = 64, nullable = false)
    private String departamentRepresent;
    
    @Column(name = "CountryRepresent", length = 64, nullable = false)
    private String countryRepresent;
    
    @Column(name = "PhoneRepresent", length = 64, nullable = false)
    private String phoneRepresent;
    
	public Long getDocNumber() {
		return id;
	}

	public void setDocNumber(Long id) {
		this.id = id;
	}

	public Long getTypeDocument() {
		return typeDocument;
	}

	public void setTypeDocument(Long typeDocument) {
		this.typeDocument = typeDocument;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDepartament() {
		return departament;
	}

	public void setDepartament(String departament) {
		this.departament = departament;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getDocNumberRepresent() {
		return docNumberRepresent;
	}

	public void setDocNumberRepresent(Long docNumberRepresent) {
		this.docNumberRepresent = docNumberRepresent;
	}

	public Long getTypeDocumentRepresent() {
		return typeDocumentRepresent;
	}

	public void setTypeDocumentRepresent(Long typeDocumentRepresent) {
		this.typeDocumentRepresent = typeDocumentRepresent;
	}

	public String getNameRepresent() {
		return nameRepresent;
	}

	public void setNameRepresent(String nameRepresent) {
		this.nameRepresent = nameRepresent;
	}

	public String getAdressRepresent() {
		return adressRepresent;
	}

	public void setAdressRepresent(String adressRepresent) {
		this.adressRepresent = adressRepresent;
	}

	public String getCityRepresent() {
		return cityRepresent;
	}

	public void setCityRepresent(String cityRepresent) {
		this.cityRepresent = cityRepresent;
	}

	public String getDepartamentRepresent() {
		return departamentRepresent;
	}

	public void setDepartamentRepresent(String departamentRepresent) {
		this.departamentRepresent = departamentRepresent;
	}

	public String getCountryRepresent() {
		return countryRepresent;
	}

	public void setCountryRepresent(String countryRepresent) {
		this.countryRepresent = countryRepresent;
	}

	public String getPhoneRepresent() {
		return phoneRepresent;
	}

	public void setPhoneRepresent(String phoneRepresent) {
		this.phoneRepresent = phoneRepresent;
	}
 
 

}