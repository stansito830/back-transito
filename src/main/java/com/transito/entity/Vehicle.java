package com.transito.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "VEHICLE")
public class Vehicle {
	  	
		@Id
		@Column(name = "carPlate", length = 150, nullable = false)
	    private String id;       
	 
	    @Column(name = "Motor", nullable = false)
	    private Long motor;
	 
	    @Column(name = "Chassis", length = 64, nullable = false)
	    private String chassis;
	    
	    @Column(name = "Model", length = 64, nullable = false)
	    private String model;
	      
		@Column(name = "enrollmentDate", nullable = false)
		private LocalDateTime creationDate;
	    
	    @Column(name = "SeatedPassengers", length = 64, nullable = false)
	    private Integer seatedPassengers;
	    
	    @Column(name = "StandingPassengers ", length = 64, nullable = false)
	    private Integer standingPassengers;
	    
	    @Column(name = "DryWeight", length = 64, nullable = false)
	    private String dryWeight;
	    
	    @Column(name = "GrossWeight", length = 64, nullable = false)
	    private String grossWeight;
	    
	    @Column(name = "NumberOfDoors", length = 64, nullable = false)
	    private Integer numberOfDoors;
	    
	    @Column(name = "brand", length = 64, nullable = false)
	    private String brand;
	    
	    @Column(name = "Line", length = 64, nullable = false)
	    private String line;

	    @ManyToOne()
		public Company company;
	    
		public Company getCompany() {
			return company;
		}

		public void setCompany(Company company) {
			this.company = company;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public Long getMotor() {
			return motor;
		}

		public void setMotor(Long motor) {
			this.motor = motor;
		}

		public String getChassis() {
			return chassis;
		}

		public void setChassis(String chassis) {
			this.chassis = chassis;
		}

		public String getModel() {
			return model;
		}

		public void setModel(String model) {
			this.model = model;
		}

		public LocalDateTime getCreationDate() {
			return creationDate;
		}

		public void setCreationDate(LocalDateTime creationDate) {
			this.creationDate = creationDate;
		}

		public Integer getSeatedPassengers() {
			return seatedPassengers;
		}

		public void setSeatedPassengers(Integer seatedPassengers) {
			this.seatedPassengers = seatedPassengers;
		}

		public Integer getStandingPassengers() {
			return standingPassengers;
		}

		public void setStandingPassengers(Integer standingPassengers) {
			this.standingPassengers = standingPassengers;
		}

		public String getDryWeight() {
			return dryWeight;
		}

		public void setDryWeight(String dryWeight) {
			this.dryWeight = dryWeight;
		}

		public String getGrossWeight() {
			return grossWeight;
		}

		public void setGrossWeight(String grossWeight) {
			this.grossWeight = grossWeight;
		}

		public Integer getNumberOfDoors() {
			return numberOfDoors;
		}

		public void setNumberOfDoors(Integer numberOfDoors) {
			this.numberOfDoors = numberOfDoors;
		}

		public String getBrand() {
			return brand;
		}

		public void setBrand(String brand) {
			this.brand = brand;
		}

		public String getLine() {
			return line;
		}

		public void setLine(String line) {
			this.line = line;
		}
	    
	    
	    
}
