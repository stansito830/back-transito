package com.transito.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;import com.transito.entity.Company;

public interface CompanyRepository extends JpaRepository<Company, String>{
	List<Company> findById(Long id);
}
