package com.transito.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transito.entity.Company;
import com.transito.entity.Driver;

public interface DriverRepository extends JpaRepository<Driver, String>{
	List<Driver> findById(Long id); 
}
