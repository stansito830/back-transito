package com.transito.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transito.entity.Driver;
import com.transito.entity.Vehicle;

public interface VehicleRepository  extends JpaRepository<Vehicle, String>{
	Optional<Vehicle> findById(String id); 
}
