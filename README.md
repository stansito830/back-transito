## proyecto-Transito Back

Este proyecto se generó con [Java ] y con apoyo de [Spring Boot JPA + H2]
### Alternativa IDE
- Clonar el proyecto mediante el comando **git clone https://gitlab.com/stansito830/back-transito.git**
- Dentro del Ide Eclipse se debera abrir la carpeta anteriormente clonada dentro del menu **file / Open projects from file System** y automaticamente Eclipse detectara el proyecto de srpingBoot. dar aceptar y el IDE se encargara de descargar las dependencias del proyecto.
- La aplicacion puede generar un conlficto con JAVA SE por lo que se debera configurar dentro del BuildPath. en las propiedades del proyecto / Java Build Path/ Pestaña libraries/ Editando la opcion JRE System librarys [JAVASe]. Se debera seleccionara Java SE 1.8 (jre1.8.0_251)
- En tal caso de que no genere errores en la consola. se podra desplegar sin inconvenientes la aplicacion asi:
-Ir al bootDashboard y ejecutar la aplicacion Spring
--Automaticamente ejecutara la aplicacion Spring y ya podra se consumida por el Front.

### Alternativa Portable
- si lo que se necesita es la aplicacion portable se puede ejecutar a traves de la consola de Windows, descargando el archivo **JAR** del repositorio que se aloja en el directorio principal, o mediante la url **https://gitlab.com/stansito830/back-transito/-/blob/78771c2c254d564ae5fb6e99a65768c593ef7345/transito-0.0.1-SNAPSHOT.jar**
- para ejecutar el archivo se debe abrir una consola de Windows y ejecutar en la ruta del archivo el comandp **java -jar transito-0.0.1-SNAPSHOT.jar**
--Automaticamente ejecutara la aplicacion Spring y ya podra se consumida por el Front.

### Nota
La aplicacion de spring boot debera estar arriba antes de subir la aplicacion front.

